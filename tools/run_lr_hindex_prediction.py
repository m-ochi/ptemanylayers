#!/usr/local/bin/python
# -*- coding: utf-8 -*-

'''
Created on Jan 23, 2019
将来的なh-index予測を行う
h-indexの定義
h-index(f) = max_i min (f(i),i)

@author: ochi
'''

import collections as cl
import codecs
import sys

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sklearn.model_selection as sms
import os
import shutil
import csv
import random
import numpy as np
import sklearn.linear_model as slm
import sklearn.metrics as smt
import sklearn.metrics.pairwise as smp
import sklearn.neighbors as sn
import scipy.stats as ss
import seaborn as sns

def runAll():
#    cvs = [i for i in range(0,5,1)]
    cvs = [0]
#    iters = ["1000000","100000","10000","1000","100","10","1"]
#    iters = ["10000"]
    iters = ["3000"]
#    orders = ["1","2","joint"]
    orders = ["1"]
#    rhos = ["0.001","0.005","0.01","0.05","0.1","0.2","0.3","0.4","0.5"]
    rhos = ["0.05"]
    for cv in cvs:
        for order in orders:
            for itr in iters:
                for rho in rhos:
                    print "order:%s,iter:%s,rho:%s"%(order,itr,rho)
                    run(cv,order,itr,rho)
    return

def run(cv=0,order='1',itr='1',rho='0.1'):
#    th = "0.200000"
    th = "0.001000"
    train_file   = "../../scopus_xml_scraper/csv01/top%sornot_hindex2013.csv"%(th)
    test_file    = "../../scopus_xml_scraper/csv01/top%sornot_hindex2016.csv"%(th)
    compare_results_file    = "../../scopus_xml_scraper/csv01/top%sornot_hindex2013.csv"%(th)
#    selectedauthors_file = "../../scopus_xml_scraper/csv01/select1authors_hindex2009.csv"
    selectedauthors_file = "../../scopus_xml_scraper/csv01/select10authors_hindex2009.csv"
#    selectedauthors_file = "../../scopus_xml_scraper/csv01/select100authors_hindex2009.csv"
#    selectedauthors_file = "../../scopus_xml_scraper/csv01/select1000authors_hindex2009.csv"
#    selectedauthors_file = "../../scopus_xml_scraper/csv01/select10000authors_hindex2009.csv"
    if order == "joint":
        vectorfileDic = {
            "non_2009":[ "../res01citation/non_all_vec_%sst_%s_%s_wo_norm_until2009.txt"%("1",itr,rho),
                         "../res01citation/non_all_vec_%sst_%s_%s_wo_norm_until2009.txt"%("2",itr,rho),
                       ],
            "non_2013":[ "../res01citation/non_all_vec_%sst_%s_%s_wo_norm_until2013.txt"%("1",itr,rho),
                         "../res01citation/non_all_vec_%sst_%s_%s_wo_norm_until2013.txt"%("2",itr,rho),
                       ]
        }
    else:
        vectorfileDic = {
            "non_2009":"../res01sample_linkprd_new_sel1_normalized/non_all_vec_%s_%sst_%s_%s_wo_norm_until2009.txt"%(cv,order,itr,rho),
            "non_2013":"../res01sample_linkprd_new_sel1_normalized/non_all_vec_%s_%sst_%s_%s_wo_norm_until2013.txt"%(cv,order,itr,rho),
#            "pre_2009":"../res01sample_linkprd_new/pre_all_vec_%s_%sst_%s_%s_wo_norm_until2009.txt"%(cv,order,itr,rho),
#            "pre_2013":"../res01sample_linkprd_new/pre_all_vec_%s_%sst_%s_%s_wo_norm_until2013.txt"%(cv,order,itr,rho),
#            "non_2009":"../res01citation/non_all_vec_%sst_%s_%s_wo_norm_until2009.txt"%(order,itr,rho),
#            "non_2013":"../res01citation/non_all_vec_%sst_%s_%s_wo_norm_until2013.txt"%(order,itr,rho),
        }

    selected_authors = readSelectedAuthorsFile(selectedauthors_file)
    train_labelDic = readLabeledFile(train_file)
    test_labelDic  = readLabeledFile(test_file)
    compare_labelDic = readLabeledFile(compare_results_file)

    test_probDic={}
    test_yDic={}
#    print "Start: %s"%(a_mode)
    if order == "joint":
        train_vec_files = vectorfileDic["non_2009"]
        train_vecDic = readVectorFilesJoint(train_vec_files)
    else:
        train_vec_file = vectorfileDic["non_2009"]
#        train_vec_file = vectorfileDic["pre_2009"]
        train_vecDic = readVectorFile(train_vec_file)

    print "complete getting train vectors"
    if order == "joint":
        test_vec_files = vectorfileDic["non_2013"]
        test_vecDic = readVectorFilesJoint(test_vec_files)
    else:
        test_vec_file = vectorfileDic["non_2013"]
#        test_vec_file = vectorfileDic["pre_2013"]
        test_vecDic = readVectorFile(test_vec_file)

    train_aids = list(set(train_vecDic.keys())&set(train_labelDic.keys()))
    test_aids  = list(set(test_vecDic.keys()) & set(test_labelDic.keys()) & set(compare_labelDic.keys()))
    selected_authors = list(set(train_aids)&set(test_aids)&set(selected_authors))

    print "complete getting test vectors"
    train_x, train_y, train_aids =  makeMTXData(train_labelDic,train_vecDic,selected_authors,train_aids)
    print "complete making train_xy"
    test_x,  test_y,  test_aids  =  makeMTXData(test_labelDic,test_vecDic,selected_authors,test_aids)
    print "complete making test_xy"
    model = slm.LogisticRegression(penalty='l2',tol=1e-6,C=1.0,warm_start=True)
    model.fit(train_x, train_y)
    print "complete lr model fit"
    print "lr model weightsk"
    print model.coef_[0,:]
    test_probs = model.predict_proba(test_x)[:,1]
#    print test_probs
#    print test_y
    for i, p in enumerate(test_probs):
        print "%s,prob:%f,y:%d"%(test_aids[i],p,test_y[i][0])
    print "complete lr model predict"

    test_compare_res = getCompareResults(test_aids, compare_results_file)
#    print "test_compare_res"
#    print test_compare_res
    test_probDic["non_2013"] = test_probs
#    test_probDic["pre_2013"] = test_probs
    test_probDic["hindex2013"] = [float(ele) for ele in test_compare_res]
    test_yDic["non_2013"]    = test_y
#    test_yDic["pre_2013"]    = test_y
    test_yDic["hindex2013"]    = test_y
#    print "model.coef_"
#    print model.coef_
    print "Complete: %s LogisticRegression"%("non_1st2013")

    printPRF(test_yDic,test_probDic)

    drawPRCMulti(test_yDic,test_probDic)

    return

def decidePth(yDic, probDic, pths=[i/100.0 for i in range(1000)]):
    pthDic = {}
    for label in probDic.keys():
        pthfscores = []
        for pth in pths:
            plabels = []
            for prob in probDic[label]:
                if prob >= pth:
                    plabels.append(1)
                else:
                    plabels.append(0)

            (precision, recall, fscore, support) = smt.precision_recall_fscore_support(yDic[label], plabels, average="binary")
            pthfscores.append((pth,fscore))

        pthfscores.sort(key=lambda x:-x[1])
        
        best_pth = pthfscores[0][0]
        best_fscore = pthfscores[0][1]
        print "%s:best pth=%.3f,fscore=%.3f"%(label,best_pth,best_fscore)
        pthDic[label] = best_pth

    return pthDic

def printPRF(yDic,probDic,pths=[i/100.0 for i in range(100)]):
    for pth in pths:
        for label in probDic.keys():
            plabels = []
            for prob in probDic[label]:
                if prob >= pth:
                    plabels.append(1)
                else:
                    plabels.append(0)

            (precision, recall, fscore, support) = smt.precision_recall_fscore_support(yDic[label], plabels, average="binary")
            print "pth=%.2f,method=%s,%f,%f,%f"%(pth,label,precision,recall,fscore)

    return

def getCompareResults(aids,compare_results_file):
    compare_labelDic = readLabeledFile(compare_results_file)
    test_compare_res = []
    for aid in aids:
        label = int(compare_labelDic[aid])
#        if label == 0:
#            label = -0.1
#        elif label == 1:
#            label = 1.1
        test_compare_res.append(label)

    return test_compare_res

def drawPRCMulti(yDic,probDic):

    num_c = 24
    linestyles = ['-','--','-.',':']
    palette = sns.color_palette("colorblind",num_c)
    fig = plt.figure(figsize=(8,8))
    ax = fig.add_subplot(111)
    lw = 2

    for i, name in enumerate(probDic.keys()):
        probs = probDic[name]
        y     = yDic[name]
        if len(set(probs)) > 2:
            precision, recall, thresholds = smt.precision_recall_curve(y,probs)
            ax.step(recall, precision, color=palette[i%num_c], alpha=0.8, where='post', label=name, lw=lw*0.5, linestyle=linestyles[i%len(linestyles)])
        else:
            precision = smt.precision_score(y,probs)
            recall    = smt.recall_score(y,probs)
            ax.step(recall, precision, 'x', color=palette[i%num_c], alpha=0.8, where='post', label=name, lw=lw*0.5, linestyle=linestyles[i%len(linestyles)])

        print name
#        print "thresholds"
#        print thresholds
        print "precision"
        print precision
        print "recall"
        print recall

    ax.set_xlim([0.0, 1.0])
    ax.set_ylim([0.0, 1.05])
    ax.set_xlabel('Recall Rate')
    ax.set_ylabel('Precision Rate')
    ax.set_title('h-index Prediction Precision-Recall Results')
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, loc='upper left', bbox_to_anchor=(1.04,1.0))
#    fig.show()
#    plt.show()
#    fig.savefig("hindex_prediction_precision_recall_curve_many_results.png", dpi=100, bbox_extra_artists=(lgd,), bbox_inches='tight')
    fig.savefig("hindex_prediction_precision_recall_curve_many_results.pdf", dpi=100, bbox_extra_artists=(lgd,), bbox_inches='tight')
    fig.clf()
    plt.close()

    return

def readVectorFilesJoint(vec_files, delimiter=' '):
    vecDics = []
    set_nodes = set([])
    for vec_file in vec_files:
        vecDic = readVectorFile(vec_file)
        vecDics.append(vecDic)
        if len(set_nodes) == 0:
            set_nodes = set(vecDic.keys())
        else:
            set_nodes &= set(vecDic.keys())

    m_vecDic = {}
    for node in list(set_nodes):
        new_vec = []
        for vecDic in vecDics:
            vec = vecDic[node]
            new_vec = np.append(new_vec,vec)

        m_vecDic[node] = new_vec

    return m_vecDic

def readVectorFile(vec_file, delimiter=' '):
    vecDic = {}
    with open(vec_file, 'r') as f:
        reader = csv.reader(f, delimiter=delimiter)
        for i, row in enumerate(reader):
            if i == 0:
                # header line
                continue
            else:
                node = row[0]
#                print len(row[1:-1])
#                for ele in row[1:-1]:
#                    print ele
#                    float(ele)
#                veclist = [float(ele) for ele in row[1:-1]]
                vec = np.array(row[1:-1],dtype=np.float)
                vecDic[node] = vec
            pass
    return vecDic

def makeMTXData(labelDic,vecDic,selected_authors,ref_aids=[]):
    xs = []
    ys = []
#    set_ids = set(vecDic.keys())
#    aids = sorted(labelDic.keys())
    vec_keys = set(vecDic.keys())
   
    selected_aids = []
    if len(ref_aids) == 0:
        aids = list(set(vecDic.keys())&set(labelDic.keys()))
    else:
        aids = ref_aids

    for i, aid in enumerate(aids):
        if aid not in vec_keys:
            continue

        selected_aids.append(aid)

        au_vec = vecDic[aid]
        distances = []
        for t_id in selected_authors:
            t_vec = vecDic[t_id]
#            distance = calcADist([au_vec,t_vec],"euclidean")
            distance = smp.cosine_distances(au_vec,[t_vec])
#            distance = smp.cosine_similarity(au_vec,t_vec)
            distances.append(distance)

        a_x = distances
        # 近い順に並べてみる
#        a_x = sorted(distances, key=lambda x:-x)
        xs.append(a_x)
        y = int(labelDic[aid])
        ys.append(y)

    print set(ys)
    y = np.array(ys, dtype=np.int).reshape((len(ys),1))
    print y.shape
    x = np.array(xs, dtype=np.float)
    print x.shape
    return x, y, selected_aids

def calc_distances(s_vec,t_vec):
    distances = []
    x = [s_vec,t_vec]
    a_dist = calcADist(x,"euclidean")
    distances.append(a_dist)
    a_dist = calcADist(x,"manhattan")
    distances.append(a_dist)
    a_dist = calcADist(x,"chebyshev")
    distances.append(a_dist)
#    a_dist = calcADist(x,"minkowski")
#    distances.append(a_dist)
#    a_dist = calcADist(x,"wminkowski")
#    distances.append(a_dist)
#    a_dist = calcADist(x,"seuclidean")
#    distances.append(a_dist)
#    a_dist = calcADist(x,"mahalanobis")
#    distances.append(a_dist)
    return distances

def calcADist(x,mode="euclidean"):
    dist = sn.DistanceMetric.get_metric(mode)
    a_dist = dist.pairwise(x)[0,1]
    return a_dist

def readLabeledFile(afile, delimiter=','):
    labelDic = {}
    with open(afile, 'r') as f:
        reader = csv.reader(f, delimiter=delimiter)
        for i, row in enumerate(reader):
            aid = row[0]
            label = row[1]
            labelDic[aid] = label

    return labelDic

def readSelectedAuthorsFile(filepath,delimiter=','):
    authors = []
    with open(filepath, 'r') as f:
        reader = csv.reader(f, delimiter=delimiter)
        for i, row in enumerate(reader):
            aid = row[0]
            authors.append(aid)

    return authors





if __name__ == "__main__":
#    run()
    runAll()
