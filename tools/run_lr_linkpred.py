#!/usr/local/bin/python
# -*- coding: utf-8 -*-

'''
Created on Mar 08, 2016
元のネットワークから正例負例を作る

@author: ochi
'''

import collections as cl
import codecs
import sys

import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sklearn.model_selection as sms
import os
import shutil
import csv
import random
import numpy as np
import numpy.random as nr
import sklearn.linear_model as slm
import sklearn.metrics as smt
import sklearn.metrics.pairwise as smp
import sklearn.neighbors as sn
import scipy.stats as ss
import seaborn as sns

import multiprocessing as mp


def runAll():
#    cvs = [i for i in range(0,5,1)]
    cvs = [0]
#    iters = ["1000000","100000","10000","1000","100","10","1"]
    iters = ["1000"]
#    orders = ["1","2","joint"]
    orders = ["2"]
#    rhos = ["0.001","0.005","0.01","0.05","0.1","0.2","0.3","0.4","0.5"]
    rhos = ["0.001","0.01","0.1","0.2","0.3","0.4","0.5","0.9"]
#    rhos = ["0.001"]
    args = []
    for cv in cvs:
        for order in orders:
            for itr in iters:
                for rho in rhos:
                    arg = (cv,order,itr,rho)
                    args.append(arg)
    
    n_cpu = mp.cpu_count() -1
    p = mp.Pool(n_cpu)
    reses = p.map(run, args)
    
#    reses = []
#    for order in orders:
#        for itr in iters:
#            for rho in rhos:
#                res = run(arg)
#                reses.append(res)

    for res in reses:
        cv,order,itr,rho,lines = res
        for line in lines:
            print "cv=%s,order=%s,itr=%s,rho=%s,%s"%(cv,order,itr,rho,line)

    return

def run(arg):
    cv,order,itr,rho = arg
    print "order:%s,iter:%s,rho:%s"%(order,itr,rho)

    train_pairs_file   = "../data/tt_all_csv01_sample/train_%s_pairs_until2013.csv"%(cv)
    train_results_file = "../data/tt_all_csv01_sample/train_%s_results_until2013.csv"%(cv)
    valid_pairs_file   = "../data/tt_all_csv01_sample/valid_%s_pairs_until2013.csv"%(cv)
    valid_results_file = "../data/tt_all_csv01_sample/valid_%s_results_until2013.csv"%(cv)
    test_pairs_file   = "../data/tt_all_csv01_sample/test_%s_pairs_until2013.csv"%(cv)
    test_results_file = "../data/tt_all_csv01_sample/test_%s_results_until2013.csv"%(cv)
    vectorfileDic = {
            "non_2013":"../res01sample_linkprd/non_all_vec_%s_%sst_%s_%s_wo_norm_until2013.txt"%(cv,order,itr,rho),
#            "pre_1st":"../res01/pre_all_vec_1st_wo_norm.txt",
#            "non_1st":"../res01/non_all_vec_1st_wo_norm.txt",
#            "non_1st":"../res01/non_vec_1st_wo_norm.txt",
#            "non_1st":"../res02/non_vec_1st_wo_norm.txt",
#            "non_2nd":"../non_vec_2nd_wo_norm.txt",
#            "pre_1st":"../pre_vec_1st_wo_norm.txt",
#            "pre_2nd":"../pre_vec_2nd_wo_norm.txt",
            }

    train_pairs, train_results = readPairsResultsFile(train_pairs_file,train_results_file)
    valid_pairs, valid_results = readPairsResultsFile(valid_pairs_file,valid_results_file)
    test_pairs, test_results   = readPairsResultsFile(test_pairs_file,test_results_file)

    test_probDic={}
    test_yDic={}
    valid_probDic={}
    valid_yDic={}
    len_test_y = 0
    pos_rate = 0
    for a_mode in sorted(vectorfileDic.keys()):
        print "Start: %s"%(a_mode)
        vec_file = vectorfileDic[a_mode]
        vecDic = readVectorFile(vec_file)
        print "complete getting vectors"
        train_x, train_y =  makeMTXData(train_pairs,train_results,vecDic)
        len_train_y = train_y.shape[0]
        pos_c_train_y = np.sum(train_y > 0)
        pos_rate = float(pos_c_train_y)/len_train_y
        print "complete making train_xy"
        valid_x,  valid_y  =  makeMTXData(valid_pairs, valid_results, vecDic)
        len_valid_y = valid_y.shape[0]
        print "complete making valid_xy"
        test_x,  test_y  =  makeMTXData(test_pairs, test_results, vecDic)
        len_test_y = test_y.shape[0]
        print "complete making test_xy"
        model = slm.LogisticRegression(penalty='l2',tol=1e-6,C=1.0,warm_start=True)
        model.fit(train_x, train_y)
        print "complete lr model fit"

        valid_probs = model.predict_proba(valid_x)[:,1]
        test_probs = model.predict_proba(test_x)[:,1]
#        print test_probs
#        print test_y
#        for i, p in enumerate(test_probs):
#            print "%s,%s,prob:%f,y:%d"%(test_pairs[i][0],test_pairs[i][1],p,test_y[i][0])
        print "complete lr model predict"
        valid_probDic[a_mode] = valid_probs
        valid_yDic[a_mode]    = valid_y
        test_probDic[a_mode] = test_probs
        test_yDic[a_mode]    = test_y
#        print "model.coef_"
#        print model.coef_
        print "Complete: %s LogisticRegression"%(a_mode)

    valid_probDic["random"] = nr.binomial(n=1,p=pos_rate,size=len_valid_y)
    valid_yDic["random"]    = valid_y
    test_probDic["random"] = nr.binomial(n=1,p=pos_rate,size=len_test_y)
    test_yDic["random"]    = test_y

#    print test_probDic
    pthDic = decidePth(valid_yDic, valid_probDic)
    lines = printPRF(test_yDic,test_probDic,pthDic)

#    drawPRCMulti(test_yDic,test_probDic)

    return cv,order,itr,rho,lines

def decidePth(yDic, probDic, pths=[i/100.0 for i in range(1000)]):
    pthDic = {}
    for label in probDic.keys():
        pthfscores = []
        for pth in pths:
            plabels = []
            for prob in probDic[label]:
                if prob >= pth:
                    plabels.append(1)
                else:
                    plabels.append(0)

            (precision, recall, fscore, support) = smt.precision_recall_fscore_support(yDic[label], plabels, average="binary")
            pthfscores.append((pth,fscore))

        pthfscores.sort(key=lambda x:-x[1])
        
        best_pth = pthfscores[0][0]
        best_fscore = pthfscores[0][1]
        print "%s:best pth=%.3f,fscore=%.3f"%(label,best_pth,best_fscore)
        pthDic[label] = best_pth

    return pthDic

def printPRF(yDic,probDic,pthDic):
    lines = []
    for label in probDic.keys():
        pth = pthDic[label]
        plabels = []
        for prob in probDic[label]:
            if prob >= pth:
                plabels.append(1)
            else:
                plabels.append(0)

        (precision, recall, fscore, support) = smt.precision_recall_fscore_support(yDic[label], plabels, average="binary")
#        print "pth=%.2f,method=%s,%f,%f,%f"%(pth,label,precision,recall,fscore)
        line = "pth=%.2f,method=%s,%f,%f,%f"%(pth,label,precision,recall,fscore)
        lines.append(line)

    return lines

def drawPRCMulti(yDic,probDic):

    num_c = 24
    linestyles = ['-','--','-.',':']
    palette = sns.color_palette("colorblind",num_c)
    fig = plt.figure(figsize=(8,8))
    ax = fig.add_subplot(111)
    lw = 2

    for i, name in enumerate(probDic.keys()):
        probs = probDic[name]
        y     = yDic[name]
        precision, recall, _ = smt.precision_recall_curve(y,probs)
        ax.step(recall, precision, color=palette[i%num_c], alpha=0.8, where='post', label=name, lw=lw*0.5, linestyle=linestyles[i%len(linestyles)])

    ax.set_xlim([0.0, 1.0])
    ax.set_ylim([0.0, 1.05])
    ax.set_xlabel('Recall Rate')
    ax.set_ylabel('Precision Rate')
    ax.set_title('Link Prediction Precision-Recall Results')
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, loc='upper left', bbox_to_anchor=(1.04,1.0))
#    fig.show()
    plt.show()
    fig.savefig("prediction_precision_recall_curve_many_results.png", dpi=100, bbox_extra_artists=(lgd,), bbox_inches='tight')
    fig.clf()
    plt.close()

    return

def calc_distances(s_vec,t_vec):
    distances = []
    x = [s_vec,t_vec]
    a_dist = calcADist(x,"euclidean")
    distances.append(a_dist)
    a_dist = calcADist(x,"manhattan")
    distances.append(a_dist)
    a_dist = calcADist(x,"chebyshev")
    distances.append(a_dist)
#    a_dist = calcADist(x,"minkowski")
#    distances.append(a_dist)
#    a_dist = calcADist(x,"wminkowski")
#    distances.append(a_dist)
#    a_dist = calcADist(x,"seuclidean")
#    distances.append(a_dist)
#    a_dist = calcADist(x,"mahalanobis")
#    distances.append(a_dist)
    return distances

def calcADist(x,mode="euclidean"):
    dist = sn.DistanceMetric.get_metric(mode)
    a_dist = dist.pairwise(x)[0,1]
    return a_dist

def makeMTXData(pairs,results,vecDic):
    xs = []
    ys = []
    set_ids = set(vecDic.keys())
    for i, (s_id, t_id) in enumerate(pairs):
        if s_id in set_ids and t_id in set_ids:
            s_vec = vecDic[s_id]
            t_vec = vecDic[t_id]
            dis = smp.cosine_distances(s_vec.reshape((1,s_vec.shape[0])),t_vec.reshape((1,t_vec.shape[0])))[0,0]
#            dis = 1.0 - np.abs(smp.cosine_similarity(s_vec.reshape((1,s_vec.shape[0])),t_vec.reshape((1,t_vec.shape[0])))[0,0])
            distances = calc_distances(s_vec,t_vec)
            a_x = [dis] + distances
#            a_x = [dis]
#            xs.append(sim)
            xs.append(a_x)
            y = int(results[i])
#            print "dis:%f,y:%d"%(dis,y)
            ys.append(y)

    print set(ys)
    y = np.array(ys, dtype=np.int).reshape((len(ys),1))
    print y.shape
    x = np.array(xs, dtype=np.float)
    print x.shape
    return x, y

def readVectorFile(vec_file, delimiter=' '):
    vecDic = {}
    with open(vec_file, 'r') as f:
        reader = csv.reader(f, delimiter=delimiter)
        for i, row in enumerate(reader):
            if i == 0:
                # header line
                continue
            else:
                node = row[0]
#                print len(row[1:-1])
#                for ele in row[1:-1]:
#                    print ele
#                    float(ele)
#                veclist = [float(ele) for ele in row[1:-1]]
                vec = np.array(row[1:-1],dtype=np.float)
                vecDic[node] = vec
            pass
    return vecDic

def readPairsResultsFile(pairs_file,results_file):
    train_pairs = readPairsCSV(pairs_file)
    train_results = readResultsCSV(results_file)

    return train_pairs, train_results

def readResultsCSV(results_file, delimiter=' '):
    results = []
    with open(results_file, 'r') as f:
        reader = csv.reader(f, delimiter=delimiter)
        for i, row in enumerate(reader):
            res = int(row[0])
            results.append(res)

    return results

def readPairsCSV(filepath, delimiter=' '):
    rows = []
    with open(filepath, 'r') as f:
        reader = csv.reader(f, delimiter=delimiter)
        for i, row in enumerate(reader):
            s_node = row[0]
            t_node = row[1]
            rows.append((s_node,t_node))

    return rows





if __name__ == "__main__":
#    run()
    runAll()
