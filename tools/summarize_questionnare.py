#!/usr/local/bin/python
# -*- coding: utf-8 -*-

'''
Created on Mar 03, 2016
Lancersのアンケート結果をまとめる

@author: ochi
'''

import csv
import collections as cl
import codecs




#pte_resultfile = "vec_2nd_wo_norm.txt" # PTEの学習結果ファイル

datadir = "../data20160317/raw_csv/"
corps = ["nankai","keihan","hankyu","hanshin","kintetsu","shiei"]
ext = ".csv"

stationidfile  = "../data/secret/station2.1u.tsv"

corpNameDic = {
    "nankai": "南海",
    "keihan": "京阪",
    "hankyu": "阪急",
    "hanshin": "阪神",
    "kintetsu": "近鉄",
    "shiei": "大阪市交",
}

corpAnsStDic = {
    "nankai": {
        "2":"堺東",
        "3":"天下茶屋",
        "4":"新今宮",
        "5":"難波",
    },
    "keihan": {
        "2":"丹波橋",
        "3":"京橋",
        "4":"北浜",
        "5":"天満橋",
        "6":"守口市",
        "7":"寝屋川市",
        "8":"枚方市",
        "9":"樟葉",
        "10":"淀屋橋",
        "11":"祇園四条",
        "12":"萱島",
        "13":"門真市",
        "14":"香里園",
        "15":"三条",
        "16":"出町柳",
    },
    "hankyu": {
        "2":"上新庄",
        "3":"六甲",
        "4":"十三",
        "5":"南方",
        "6":"南茨木",
        "7":"塚口",
        "8":"夙川",
        "9":"天神橋筋六丁目",
        "10":"宝塚",
        "11":"川西能勢口",
        "12":"桂",
        "13":"梅田",
        "14":"武庫之荘",
        "15":"池田",
        "16":"河原町",
        "17":"淡路",
        "18":"烏丸",
        "19":"石橋",
        "20":"神戸三宮",
        "21":"茨木市",
        "22":"蛍池",
        "23":"西宮北口",
        "24":"豊中",
        "25":"高槻市",
    },
    "hanshin": {
        "2":"尼崎",
        "3":"梅田",
        "4":"甲子園",
        "5":"神戸三宮",
        "6":"西宮",
    },
    "kintetsu": {
        "2":"京都",
        "3":"大和八木",
        "4":"大和西大寺",
        "5":"大阪上本町",
        "6":"大阪阿部野橋",
        "7":"大阪難波",
        "8":"学園前",
        "9":"布施",
        "10":"生駒",
        "11":"藤井寺",
        "12":"近鉄丹波橋",
        "13":"近鉄八尾",
        "14":"近鉄名古屋",
        "15":"近鉄奈良",
        "16":"近鉄日本橋",
        "17":"高の原",
        "18":"鶴橋",
    },
    "shiei": {
        "2":"あびこ",
        "3":"なんば",
        "4":"中津",
        "5":"中百舌鳥",
        "6":"京橋",
        "7":"住之江公園",
        "8":"北浜",
        "9":"南森町",
        "10":"四ツ橋",
        "11":"堺筋本町",
        "12":"大日",
        "13":"天下茶屋",
        "14":"天満橋",
        "15":"天王寺",
        "16":"天神橋筋六丁目",
        "17":"弁天町",
        "18":"心斎橋",
        "19":"新大阪",
        "20":"日本橋",
        "21":"本町",
        "22":"東三国",
        "23":"東梅田",
        "24":"梅田",
        "25":"江坂",
        "26":"淀屋橋",
        "27":"肥後橋",
        "28":"西中島南方",
        "29":"西梅田",
        "30":"谷町九丁目",
        "31":"谷町六丁目",
        "32":"谷町四丁目",
        "33":"都島",
        "34":"長堀橋",
        "35":"長居",
        "36":"阿波座",
    }
}

def run():
    corpNameStNameIdDic = makeCorpNameStNameIdDic(stationidfile, delimiter='\t')
#        corpName = corpNameDic[corp]
    #とりあえず集計
    for corp in corps:
        fileName = corp + ext
        filePath = datadir + fileName
        ansIdCDic = summrizefile(filePath)
#        print ansIdCDic
        ansIdQCDic = chDicKeys(ansIdCDic)
#        print ansIdQCDic
        nansIdQCDic = normalize(ansIdQCDic)
#        print nansIdQCDic
        stidimpDic, impidNameDic = matching(nansIdQCDic,corp, corpNameStNameIdDic)
#        print stidimpDic
#        print impidNameDic

        f = open("impid.csv",'w')
        writer = csv.writer(f,delimiter=' ',quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(["id","impression"])
        for impid in sorted(impidNameDic.keys()):
            name = impidNameDic[impid]
            writer.writerow([impid,name])
        f.close()

        f = open("st-imp.csv",'w')
        writer = csv.writer(f,delimiter='\t',quoting=csv.QUOTE_NONNUMERIC)
        for stid in sorted(stidimpDic.keys()):
            for impid in sorted(stidimpDic[stid].keys()):
                a_val = stidimpDic[stid][impid]
                if a_val <= 0.01:
                    continue
                else:
                    writer.writerow([stid,impid,"%.3f"%(a_val)])
        f.close()

    pass

def matching(nansIdQCDic,corp,corpNameStNameIdDic):
    stidimpDic = {}
    impidNameDic = {}
    impNameIdDic = {}

    ansid0 = nansIdQCDic.keys()[0]
    imps = sorted(nansIdQCDic[ansid0])
    for i, imp in enumerate(imps):
        impid = "i"+str(i)
        impidNameDic[impid] = imp
        impNameIdDic[imp] = impid

    corpName = corpNameDic[corp]

    for ansid in nansIdQCDic.keys():
        if ansid not in corpAnsStDic[corp].keys():
            continue

        ansStName = corpAnsStDic[corp][ansid]
        stid = corpNameStNameIdDic[corpName][ansStName]
        stidimpDic[stid] = {}
        for imp in imps:
            impid = impNameIdDic[imp]
            stidimpDic[stid][impid] =  nansIdQCDic[ansid][imp]

    return stidimpDic, impidNameDic

def normalize(ansIdQCDic):
    nansIdQCDic = {}
    for key1 in ansIdQCDic.keys():
        nansIdQCDic[key1] = {}
        sums = 0
        for key2 in ansIdQCDic[key1].keys():
            sums += ansIdQCDic[key1][key2]

        for key2 in ansIdQCDic[key1].keys():
            a_val = ansIdQCDic[key1][key2]
            nansIdQCDic[key1][key2] = a_val / float(sums)

    return nansIdQCDic

def chDicKeys(ansIdCDic):
    key1s = sorted(ansIdCDic.keys())
    allkey2s = []
    for key1 in key1s:
        allkey2s.extend(ansIdCDic[key1].keys())

    setAllKey2s = sorted(list(set(allkey2s)))

    ansIdQCDic = {}
    for key2 in setAllKey2s:
        ansIdQCDic[key2] = {}
        for key1 in key1s:
            ansIdQCDic[key2][key1] = 0

    for key1 in key1s:
        for key2 in ansIdCDic[key1].keys():
            a_val = ansIdCDic[key1][key2]
            ansIdQCDic[key2][key1] += a_val

    return ansIdQCDic

def summrizefile(filePath):
    ansIdCDic = {}
    columns = ["nearest","daily","trans1","trans2","trans3","shopping1","shopping2","shopping3","eating1","eating2","eating3","playing1","playing2","playing3"]
    f = codecs.open(filePath, 'r', "utf_8")
    reader = csv.DictReader(f, delimiter=',')
    for rdic in reader:
        for column in columns:
            if column not in ansIdCDic.keys():
                ansIdCDic[column] = cl.defaultdict(int)

            ansIdCDic[column][rdic[column]] += 1

    f.close()
    s_ansIdCDic = mergeColumns(ansIdCDic, "trans", ["trans1","trans2","trans3"])
    s_ansIdCDic = mergeColumns(s_ansIdCDic, "shopping", ["shopping1","shopping2","shopping3"])
    s_ansIdCDic = mergeColumns(s_ansIdCDic, "eating", ["eating1","eating2","eating3"])
    s_ansIdCDic = mergeColumns(s_ansIdCDic, "playing", ["playing1","playing2","playing3"])

    return s_ansIdCDic

def mergeColumns(ansIdCDic, cName, m_columns):
    s_ansIdCDic = {}
    for akey in ansIdCDic.keys():
        if akey not in m_columns:
            s_ansIdCDic[akey] = ansIdCDic[akey]
        else:
            if cName not in s_ansIdCDic.keys():
                s_ansIdCDic[cName] = cl.defaultdict(int)

            for ansid in ansIdCDic[akey].keys():
                s_ansIdCDic[cName][ansid] += ansIdCDic[akey][ansid]

    return s_ansIdCDic

def makeCorpNameStNameIdDic(filename,delimiter=',',header=True):
    a_dic = {}
    f = codecs.open(filename, 'r', "utf_8")
    reader = csv.reader(f, delimiter=delimiter)
    for i, row in enumerate(reader):
        if i == 0 and header is True:
            # 最初の行はヘッダ
            continue
        else:
            st_id  = row[0]
#            print new_id
            st_name = row[1]
            corp_name = row[2]
            if corp_name not in a_dic.keys():
                a_dic[corp_name] = {}

            a_dic[corp_name][st_name] = st_id
    f.close()

    return a_dic



######################
def makeIdNameDicFromCSV(filename,delimiter=',',header=True):
    a_dic = {}
    f = codecs.open(filename, 'r', "utf_8")
    reader = csv.reader(f, delimiter=delimiter)
    for i, row in enumerate(reader):
        if i == 0 and header is True:
            # 最初の行はヘッダ
            continue
        else:
            new_id  = row[0]
#            print new_id
            a_val = str(row[1])
            a_dic[new_id] = a_val
    f.close()

    return a_dic

def makeIdNameDicFromCSVstation(filename,delimiter=',',header=True):
    a_dic = {}
    f = codecs.open(filename, 'r', "utf_8")
    reader = csv.reader(f, delimiter=delimiter)
    for i, row in enumerate(reader):
        if i == 0 and header is True:
            # 最初の行はヘッダ
            continue
        else:
            new_id  = row[0]
#            print new_id
            a_val = str(row[2])+"・"+str(row[1])
            a_dic[new_id] = a_val
    f.close()

    return a_dic

def getIdVec():
    idVecDic = {}
    f = codecs.open(pte_resultfile, 'r', "utf_8")
    reader = csv.reader(f, delimiter=' ')
    for i, row in enumerate(reader):
        if i == 0:
            # 最初の行はヘッダ
            continue
        else:
            new_id  = row[0]
#            print new_id
#            print row[1:]
            #なぜか一番後ろに空白があるんで
            a_vec = np.array(row[1:-1], dtype=np.float)
            idVecDic[new_id] = a_vec
    f.close()

    return idVecDic

def getNearest(a_id, idVecDic, idcorpdic, idimpdic, bestN=10):
    a_vec = idVecDic[a_id]
    idDistanceDic = {}
    ids = idVecDic.keys()
    for b_id in ids:
        if b_id == a_id:
            continue
        else:
            b_vec = idVecDic[b_id]
            dist = spd.euclidean(b_vec,a_vec)
            idDistanceDic[b_id] = dist

    nears = []
    for k,v in sorted(idDistanceDic.items(), key=lambda x: x[1]):
#        if k not in idcorpdic.keys() and k not in idimpdic.keys():
#            nears.append(k)
        nears.append(k)
    return nears[:bestN]

def run0():
    # csv読み込み
    idcorpdic     = makeIdNameDicFromCSV(corpidfile)
#    print "a"
    idimpdic      = makeIdNameDicFromCSV(impidfile)
#    print "b"
    idstationdic  = makeIdNameDicFromCSVstation(stationidfile, delimiter='\t')
#    print "c"

    idNameDic = {}
    idNameDic.update(idcorpdic)
    idNameDic.update(idimpdic)
    idNameDic.update(idstationdic)
#    print idNameDic

    # PTEの学習結果のベクトル取得
    idVecDic = getIdVec()

    # impressionの近傍のIDを取得
    new_ids = idVecDic.keys()
#    print new_ids
    plotOrNot = [False]*len(new_ids)
    for a_id in idimpdic.keys():
#        print a_id
#        print type(a_id)
#        print type(new_ids[0])
#        for i, new_id in enumerate(new_ids):
#            print "%d:%s"%(i,new_id)
#            print type(new_id)
        idx = new_ids.index(a_id)
    
        plotOrNot[idx] = True
        nears = getNearest(a_id, idVecDic, idcorpdic, idimpdic, bestN=bestN)
        print "%sに近いベクトル"%(idimpdic[a_id])
        for i,near in enumerate(nears):
            idx = new_ids.index(near)
            print "%d, %s"%(i+1, idNameDic[near])
            plotOrNot[idx] = True
        print ""


    # 獲得した特徴量の可視化(TSNE method)
    names = [ idNameDic[new_id] for new_id in new_ids ]
    X = np.array(idVecDic.values())
    model = sm.TSNE(n_components=2, random_state=0)
    resX = model.fit_transform(X)
    draw2DScatterPlot(new_ids, names,resX,plotOrNot,"PTE_Result")


def draw2DScatterPlot(new_ids, vocab, reduce_vecs, plotOrNot, parameterStr):
    matplotlib.rcParams['axes.unicode_minus'] = False
    fig = pyplot.figure()
    ax = fig.add_subplot(111)

    # 軸ラベルの設定
    ax.set_xlabel("1st component")
    ax.set_ylabel("2nd component")

    # 表示範囲の設定
    min_val_x = np.min(reduce_vecs[:,0]) -0.1
    min_val_y = np.min(reduce_vecs[:,1]) -0.1
    max_val_x = np.max(reduce_vecs[:,0]) +0.1
    max_val_y = np.max(reduce_vecs[:,1]) +0.1
    print min_val_x
    print max_val_x

    ax.set_xlim(min_val_x,max_val_x)
    ax.set_ylim(min_val_y,max_val_y)

    #plot
#    ax.plot(reduce_vecs[:,0], reduce_vecs[:,1], "o", color="#cccccc", ms=4, mew=0.5)

    f = open(tSNEFile, "w")
    writer = csv.writer(f, delimiter=',', quoting=csv.QUOTE_NONNUMERIC)
    for i, w in enumerate(vocab):
        xy = reduce_vecs[i,:]
        writer.writerow([w,reduce_vecs[i,0],reduce_vecs[i,1]])
#        ax.plot(reduce_vecs[i,0], reduce_vecs[i,1], "o", color="#cccccc", ms=4, mew=0.5)
#        ax.annotate(w,xy=xy, xytext=(1,1), textcoords="offset points")
        if plotOrNot[i] is True:
#        if new_ids[i] in  set(["0860","0834","0513","0962"]) or plotOrNot[i] is True:
            ax.plot(reduce_vecs[i,0], reduce_vecs[i,1], "o", color="#ff0000", ms=4, mew=0.5)
            ax.annotate(w,xy=xy, xytext=(1,1), textcoords="offset points")
        else:
            ax.plot(reduce_vecs[i,0], reduce_vecs[i,1], "o", color="#cccccc", ms=4, mew=0.5)
#            if i % 15 == 0: 
#                ax.annotate(w,xy=xy, xytext=(1,1), textcoords="offset points")

#        if i % 15 == 0: 
#            ax.annotate(w,xy=xy, xytext=(1,1), textcoords="offset points")
#        else:
#            ax.annotate('',xy=xy, xytext=(1,1), textcoords="offset points")
    pyplot.show()
    f.close()
#    pyplot.savefig("pic"+parameterStr+".png")
#    pyplot.close()

    return


if __name__ == "__main__":
    run()
