#!/bin/bash

#header="../results/early_lock2d100k/vec_2nd_wo_norm.txt"
#header="../src/vec_2nd_wo_norm.txt"
header="../movie/non_vec_1st_wo_norm.txt"
#header="../vec_2nd_wo_norm.txt"
times=100
#nmax=70

for i in `seq 0 ${times}`
do
    if [ ${i} = 0 ]; then
        n='0'
    else
        n=${i}'00000'
    fi

    file=${header}${n}
    echo $file
    python2.7 make_tsne_fig.py ${file} ${n}
done

#convert -layers optimize -loop 0 -delay 240 pic_out00000000.png -delay 40 pic_out*png -delay 240 pic_out10000000.png anim.gif

#for i in `seq 1 ${times}`
#do
#    if [ ${i} = 1 ]; then
#        n=''
#    else
#        n='1'
#    fi
#
#    for j in `seq 1 ${i}`
#    do
#        n=${n}'0'
#    done
#    file=${header}${n}
#    echo $file
#    python2.7 make_tsne_fig.py ${file} ${n}
#done

