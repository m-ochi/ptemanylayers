#!/usr/local/bin/python
# -*- coding: utf-8 -*-

'''
Created on Mar 08, 2016
元のネットワークから正例負例を作る

@author: ochi
'''

import collections as cl
import codecs
import sys

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sklearn.model_selection as sms
import os
import shutil
import csv
import random
import numpy as np
import sklearn.linear_model as slm
import sklearn.metrics as smt
import sklearn.metrics.pairwise as smp
import sklearn.neighbors as sn
import scipy.stats as ss
import seaborn as sns


def run():
    train_pairs_file   = "../data/tt_csv01/train_pairs.csv"
    train_results_file = "../data/tt_csv01/train_results.csv"
    test_pairs_file    = "../data/tt_csv01/test_pairs.csv"
    test_results_file  = "../data/tt_csv01/train_results.csv"
    vectorfileDic = {
            "non_1st":"../non_vec_1st_wo_norm.txt",
#            "non_2nd":"../non_vec_2nd_wo_norm.txt",
#            "pre_1st":"../pre_vec_1st_wo_norm.txt",
#            "pre_2nd":"../pre_vec_2nd_wo_norm.txt",
            }

    train_pairs, train_results = readPairsResultsFile(train_pairs_file,train_results_file)
    test_pairs, test_results   = readPairsResultsFile(test_pairs_file,test_results_file)

    test_probDic={}
    test_yDic={}
    for a_mode in sorted(vectorfileDic.keys()):
        print "Start: %s"%(a_mode)
        vec_file = vectorfileDic[a_mode]
        vecDic = readVectorFile(vec_file)
        print "complete getting vectors"
        train_x, train_y =  makeMTXData(train_pairs,train_results,vecDic)
        print "complete making train_xy"
        test_x,  test_y  =  makeMTXData(test_pairs, test_results, vecDic)
        print "complete making test_xy"
        model = slm.LogisticRegression(penalty='l2',tol=1e-6,C=1.0,warm_start=True)
        model.fit(train_x, train_y)
        print "complete lr model fit"
        test_probs = model.predict_proba(test_x)[:,1]
        print test_probs
        print test_y
        for i, p in enumerate(test_probs):
            print "prob:%f,y:%d"%(p,test_y[i][0])
        print "complete lr model predict"
        test_probDic[a_mode] = test_probs
        test_yDic[a_mode]    = test_y
#        print "model.coef_"
#        print model.coef_
        print "Complete: %s LogisticRegression"%(a_mode)

    drawPRCMulti(test_yDic,test_probDic)

    return

def drawPRCMulti(yDic,probDic):

    num_c = 24
    linestyles = ['-','--','-.',':']
    palette = sns.color_palette("colorblind",num_c)
    fig = plt.figure(figsize=(8,8))
    ax = fig.add_subplot(111)
    lw = 2

    for i, name in enumerate(probDic.keys()):
        probs = probDic[name]
        y     = yDic[name]
        precision, recall, _ = smt.precision_recall_curve(y,probs)
        ax.step(recall, precision, color=palette[i%num_c], alpha=0.8, where='post', label=name, lw=lw*0.5, linestyle=linestyles[i%len(linestyles)])

    ax.set_xlim([0.0, 1.0])
    ax.set_ylim([0.0, 1.05])
    ax.set_xlabel('Recall Rate')
    ax.set_ylabel('Precision Rate')
    ax.set_title('Link Prediction Precision-Recall Results')
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, loc='upper left', bbox_to_anchor=(1.04,1.0))
    fig.savefig("prediction_precision_recall_curve_many_results.png", dpi=100, bbox_extra_artists=(lgd,), bbox_inches='tight')
    fig.clf()
    plt.close()

    return

def calc_distances(s_vec,t_vec):
    distances = []
    x = [s_vec,t_vec]
    a_dist = calcADist(x,"euclidean")
    distances.append(a_dist)
    a_dist = calcADist(x,"manhattan")
    distances.append(a_dist)
    a_dist = calcADist(x,"chebyshev")
    distances.append(a_dist)
#    a_dist = calcADist(x,"minkowski")
#    distances.append(a_dist)
#    a_dist = calcADist(x,"wminkowski")
#    distances.append(a_dist)
#    a_dist = calcADist(x,"seuclidean")
#    distances.append(a_dist)
#    a_dist = calcADist(x,"mahalanobis")
#    distances.append(a_dist)
    return distances

def calcADist(x,mode="euclidean"):
    dist = sn.DistanceMetric.get_metric(mode)
    a_dist = dist.pairwise(x)[0,1]
    return a_dist

def makeMTXData(pairs,results,vecDic):
    xs = []
    ys = []
    set_ids = set(vecDic.keys())
    for i, (s_id, t_id) in enumerate(pairs):
        if s_id in set_ids and t_id in set_ids:
            s_vec = vecDic[s_id]
            t_vec = vecDic[t_id]
            sim = smp.cosine_similarity(s_vec.reshape((1,s_vec.shape[0])),t_vec.reshape((1,t_vec.shape[0])))[0,0]
            distances = calc_distances(s_vec,t_vec)
            a_x = [sim] + distances
#            xs.append(sim)
            xs.append(a_x)
            y = int(results[i])
            print "sim:%f,y:%d"%(sim,y)
            ys.append(y)

#    x = np.array(xs,dtype=np.float).reshape((len(xs),1))
    x = np.array(xs,dtype=np.float).reshape((len(xs),len(xs[0])))
    print x.shape
    y = np.array(ys, dtype=np.int).reshape((len(ys),1))
    print y.shape
    return x, y

def readVectorFile(vec_file, delimiter=' '):
    vecDic = {}
    with open(vec_file, 'r') as f:
        reader = csv.reader(f, delimiter=delimiter)
        for i, row in enumerate(reader):
            if i == 0:
                # header line
                continue
            else:
                node = row[0]
#                print len(row[1:-1])
#                for ele in row[1:-1]:
#                    print ele
#                    float(ele)
#                veclist = [float(ele) for ele in row[1:-1]]
                vec = np.array(row[1:-1],dtype=np.float)
                vecDic[node] = vec
            pass
    return vecDic

def readPairsResultsFile(pairs_file,results_file):
    train_pairs = readPairsCSV(pairs_file)
    train_results = readResultsCSV(results_file)

    return train_pairs, train_results

def readResultsCSV(results_file, delimiter=' '):
    results = []
    with open(results_file, 'r') as f:
        reader = csv.reader(f, delimiter=delimiter)
        for i, row in enumerate(reader):
            res = int(row[0])
            results.append(res)

    return results

def readPairsCSV(filepath, delimiter=' '):
    rows = []
    with open(filepath, 'r') as f:
        reader = csv.reader(f, delimiter=delimiter)
        for i, row in enumerate(reader):
            s_node = row[0]
            t_node = row[1]
            rows.append((s_node,t_node))

    return rows





if __name__ == "__main__":
    run()
