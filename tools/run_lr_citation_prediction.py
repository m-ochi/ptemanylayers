#!/usr/local/bin/python
# -*- coding: utf-8 -*-

'''
Created on Jan 10, 2019
将来的な被引用数予測を行う

@author: ochi
'''

import collections as cl
import codecs
import sys

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sklearn.model_selection as sms
import os
import shutil
import csv
import random
import numpy as np
import sklearn.linear_model as slm
import sklearn.metrics as smt
import sklearn.metrics.pairwise as smp
import sklearn.neighbors as sn
import scipy.stats as ss
import seaborn as sns

def runAll():
#    cvs = [i for i in range(0,5,1)]
    cvs = [0]
#    iters = ["1000000","100000","10000","1000","100","10","1"]
    iters = ["10000"]
#    orders = ["1","2","joint"]
    orders = ["2"]
#    rhos = ["0.001","0.005","0.01","0.05","0.1","0.2","0.3","0.4","0.5"]
    rhos = ["0.2"]
    for cv in cvs:
        for order in orders:
            for itr in iters:
                for rho in rhos:
                    print "order:%s,iter:%s,rho:%s"%(order,itr,rho)
                    run(cv,order,itr,rho)
    return

def run(cv=0,order='1',itr='1',rho='0.1'):
    train_file   = "../../scopus_xml_scraper/csv01/top0.200000ornot_published2009_after3.csv"
    test_file    = "../../scopus_xml_scraper/csv01/top0.200000ornot_published2013_after3.csv"
#    selectedpapers_file = "../../scopus_xml_scraper/csv01/select1papers_published2009_after3.csv"
    selectedpapers_file = "../../scopus_xml_scraper/csv01/select10papers_published2009_after3.csv"
#    selectedpapers_file = "../../scopus_xml_scraper/csv01/select100papers_published2009_after3.csv"
#    selectedpapers_file = "../../scopus_xml_scraper/csv01/select1000papers_published2009_after3.csv"
#    selectedpapers_file = "../../scopus_xml_scraper/csv01/select10000papers_published2009_after3.csv"
    vectorfileDic = {
#            "non_1st2009":"../res01citation/non_all_vec_1st_wo_norm_until2009.txt",
#            "non_1st2013":"../res01citation/non_all_vec_1st_wo_norm_until2013.txt",
            "pre_2009":"../res01sample_linkprd_new/pre_all_vec_%s_%sst_%s_%s_wo_norm_until2009.txt"%(cv,order,itr,rho),
            "pre_2013":"../res01sample_linkprd_new/pre_all_vec_%s_%sst_%s_%s_wo_norm_until2013.txt"%(cv,order,itr,rho),
            }

    selected_papers = readSelectedPapersFile(selectedpapers_file)
    print "Before len(selected_papers)"
    print len(selected_papers)
    train_labelDic = readLabeledFile(train_file)
    test_labelDic  = readLabeledFile(test_file)

    test_probDic={}
    test_yDic={}
#    print "Start: %s"%(a_mode)
    train_vec_file = vectorfileDic["pre_2009"]
    train_vecDic = readVectorFile(train_vec_file)
    print "complete getting train vectors"
    test_vec_file = vectorfileDic["pre_2013"]
    test_vecDic = readVectorFile(test_vec_file)
    print "complete getting test vectors"

    train_eids = list(set(train_vecDic.keys())&set(train_labelDic.keys()))
    test_eids  = list(set(test_vecDic.keys()) & set(test_labelDic.keys()))
    # test label等を入れるとselected_papersが０件になってしまう
#    selected_papers = list(set(train_eids)&set(test_eids)&set(selected_papers))
#    print "After len(selected_papers)"
#    print len(selected_papers)

    train_x, train_y, train_eids =  makeMTXData(train_labelDic,train_vecDic,selected_papers,train_eids)
    print "complete making train_xy"
    test_x,  test_y,  test_eids  =  makeMTXData(test_labelDic,test_vecDic,selected_papers,test_eids)
    print "complete making test_xy"
    model = slm.LogisticRegression(penalty='l2',tol=1e-6,C=1.0,warm_start=True)
    model.fit(train_x, train_y)
    print "complete lr model fit"
    test_probs = model.predict_proba(test_x)[:,1]
    print test_probs
    print test_y
    for i, p in enumerate(test_probs):
        print "%s,prob:%f,y:%d"%(test_eids[i],p,test_y[i][0])
    print "complete lr model predict"

#    test_probDic["non_1st2013"] = test_probs
#    test_yDic["non_1st2013"]    = test_y
    test_probDic["pre_2013"] = test_probs
    test_yDic["pre_2013"]    = test_y
#    print "model.coef_"
#    print model.coef_

#    test_compare_res = getCompareResults(test_aids, compare_results_file)
    test_probDic["random"] = [random.random() for ty in test_y]
    test_yDic["random"]    = test_y

    print "Complete: %s LogisticRegression"%("non_1st2013")

    printPRF(test_yDic,test_probDic)

    drawPRCMulti(test_yDic,test_probDic)

    return

def getCompareResults(aids,compare_results_file):
    compare_labelDic = readLabeledFile(compare_results_file)
    test_compare_res = []
    for aid in aids:
        label = int(compare_labelDic[aid])
        test_compare_res.append(label)

    return test_compare_res

def printPRF(yDic,probDic,pths=[i/100.0 for i in range(100)]):
    for pth in pths:
        for label in probDic.keys():
            plabels = []
            for prob in probDic[label]:
                if prob >= pth:
                    plabels.append(1)
                else:
                    plabels.append(0)

            (precision, recall, fscore, support) = smt.precision_recall_fscore_support(yDic[label], plabels, average="binary")
            print "pth=%.2f,method=%s,%f,%f,%f"%(pth,label,precision,recall,fscore)

    return

def drawPRCMulti(yDic,probDic):

    num_c = 24
    linestyles = ['-','--','-.',':']
    palette = sns.color_palette("colorblind",num_c)
    fig = plt.figure(figsize=(8,8))
    ax = fig.add_subplot(111)
    lw = 2

    for i, name in enumerate(probDic.keys()):
        probs = probDic[name]
        y     = yDic[name]
        precision, recall, _ = smt.precision_recall_curve(y,probs)
        ax.step(recall, precision, color=palette[i%num_c], alpha=0.8, where='post', label=name, lw=lw*0.5, linestyle=linestyles[i%len(linestyles)])

    ax.set_xlim([0.0, 1.0])
    ax.set_ylim([0.0, 1.05])
    ax.set_xlabel('Recall Rate')
    ax.set_ylabel('Precision Rate')
    ax.set_title('Citation Prediction Precision-Recall Results')
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, loc='upper left', bbox_to_anchor=(1.04,1.0))
#    fig.show()
    plt.show()
    fig.savefig("citation_prediction_precision_recall_curve_many_results.png", dpi=100, bbox_extra_artists=(lgd,), bbox_inches='tight')
    fig.clf()
    plt.close()

    return

def readVectorFile(vec_file, delimiter=' '):
    vecDic = {}
    with open(vec_file, 'r') as f:
        reader = csv.reader(f, delimiter=delimiter)
        for i, row in enumerate(reader):
            if i == 0:
                # header line
                continue
            else:
                node = row[0]
#                print len(row[1:-1])
#                for ele in row[1:-1]:
#                    print ele
#                    float(ele)
#                veclist = [float(ele) for ele in row[1:-1]]
                vec = np.array(row[1:-1],dtype=np.float)
                vecDic[node] = vec
            pass
    return vecDic

def makeMTXData(labelDic,vecDic,selected_papers,ref_pids=[]):
    xs = []
    ys = []
#    set_ids = set(vecDic.keys())
#    eids = sorted(labelDic.keys())
    vec_keys = set(vecDic.keys())
   
    selected_pids = []
    if len(ref_pids) == 0:
        pids = list(set(vecDic.keys())&set(labelDic.keys()))
    else:
        pids = ref_pids

    for i, eid in enumerate(pids):
        e_vec = vecDic[eid]
        selected_pids.append(eid)
        distances = []
        for t_id in selected_papers:
            t_vec = vecDic[t_id]
            distance = calcADist([e_vec,t_vec],"euclidean")
            distances.append(distance)

        a_x = distances
        xs.append(a_x)
        y = int(labelDic[eid])
        ys.append(y)

    print set(ys)
    y = np.array(ys, dtype=np.int).reshape((len(ys),1))
    print y.shape
    x = np.array(xs, dtype=np.float)
    print x.shape
    return x, y, selected_pids

def calc_distances(s_vec,t_vec):
    distances = []
    x = [s_vec,t_vec]
    a_dist = calcADist(x,"euclidean")
    distances.append(a_dist)
    a_dist = calcADist(x,"manhattan")
    distances.append(a_dist)
    a_dist = calcADist(x,"chebyshev")
    distances.append(a_dist)
#    a_dist = calcADist(x,"minkowski")
#    distances.append(a_dist)
#    a_dist = calcADist(x,"wminkowski")
#    distances.append(a_dist)
#    a_dist = calcADist(x,"seuclidean")
#    distances.append(a_dist)
#    a_dist = calcADist(x,"mahalanobis")
#    distances.append(a_dist)
    return distances

def calcADist(x,mode="euclidean"):
    dist = sn.DistanceMetric.get_metric(mode)
    a_dist = dist.pairwise(x)[0,1]
    return a_dist

def readLabeledFile(afile, delimiter=','):
    labelDic = {}
    with open(afile, 'r') as f:
        reader = csv.reader(f, delimiter=delimiter)
        for i, row in enumerate(reader):
            eid = row[0]
            label = row[1]
            labelDic[eid] = label

    return labelDic

def readSelectedPapersFile(filepath,delimiter=','):
    papers = []
    with open(filepath, 'r') as f:
        reader = csv.reader(f, delimiter=delimiter)
        for i, row in enumerate(reader):
            eid = row[0]
            papers.append(eid)

    return papers





if __name__ == "__main__":
#    run()
    runAll()
