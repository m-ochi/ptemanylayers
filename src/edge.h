//
// Created by m-ochi on 16/03/11.
//

#ifndef PTESELF_EDGE_H
#define PTESELF_EDGE_H

#include <iostream>
#include <memory>

class Edge {
public:
    Edge();
    ~Edge();
    int source_id;
    int target_id;
    double weight;
};


#endif //PTESELF_EDGE_H
