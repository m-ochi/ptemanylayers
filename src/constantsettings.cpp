//
// Created by m-ochi on 16/03/11.
//

#include "constantsettings.h"

ConstantSettings::ConstantSettings(
        std::string pp_net_a,
        std::string pw_net_a,
        std::string pa_net_a,
        std::string aa_net_a,
        std::string pj_net_a,
        std::string pretrain_wvec_a,
        std::string outputfile_a,
        int binary_a,
        int dim_a,
        int order_a,
        int negative_a,
        int64_t samples_a,
        int threads_a,
        float rho_a
)
:
        pp_net(pp_net_a),
        pw_net(pw_net_a),
        pa_net(pa_net_a),
        aa_net(aa_net_a),
        pj_net(pj_net_a),
        pretrain_wvec(pretrain_wvec_a),
        outputfile(outputfile_a),

        binary(binary_a),
        dim(dim_a),
        order(order_a),
        negative(negative_a),
        samples(samples_a),
        threads(threads_a),
        rho(rho_a)
{

    hash_table_size = 30000000;
    neg_table_size = 1e8;
    sigmoid_table_size = 1000;

}


ConstantSettings::~ConstantSettings() {}

/*
int main() {

    ConstantSettings* cs = new ConstantSettings("dataminimal/st-st.csv","dataminimal/st-corp.csv","dataminimal/st-imp.csv","vec_2nd_wo_norm.txt");
    std::unique_ptr<ConstantSettings> cs1( new ConstantSettings("dataminimal/st-st.csv","dataminimal/st-corp.csv","dataminimal/st-imp.csv","vec_2nd_wo_norm.txt") );

    std::cout << "cs->ww_net:"              << cs->ww_net               << std::endl;
    std::cout << "cs->sigmoid_table_size:"  << cs->sigmoid_table_size   << std::endl;
    std::cout << "cs1->neg_table_size:"     << cs1->neg_table_size      << std::endl;
    std::cout << "cs1->sigmoid_table_size:" << cs1->sigmoid_table_size  << std::endl;

    delete cs;

    return 0;
}
*/

