#include "pte5.h"
#include <mutex>

//#define MAX_STRING 100
//#define SIGMOID_BOUND 6
//#define NEG_SAMPLING_POWER 0.75

static std::mutex mtx;
std::unique_lock<std::mutex> uniq_lk(mtx);

// new
std::unique_ptr<ConstantSettings> PTE5::constant_settings;

// 定数群

std::unique_ptr<Network> PTE5::network_pp;
std::unique_ptr<Network> PTE5::network_pw;
std::unique_ptr<Network> PTE5::network_pa;
std::unique_ptr<Network> PTE5::network_aa;
std::unique_ptr<Network> PTE5::network_pj;
std::unique_ptr<Network> PTE5::network_all;

std::unique_ptr<PretrainedVectors> PTE5::pretrained_word_vectors;

std::unique_ptr<Samplers> PTE5::sampler_pp;
std::unique_ptr<Samplers> PTE5::sampler_pw;
std::unique_ptr<Samplers> PTE5::sampler_pa;
std::unique_ptr<Samplers> PTE5::sampler_aa;
std::unique_ptr<Samplers> PTE5::sampler_pj;

std::unique_ptr<EmbeddedVector> PTE5::emb_pp;
std::unique_ptr<EmbeddedVector> PTE5::emb_pw;
std::unique_ptr<EmbeddedVector> PTE5::emb_pa;
std::unique_ptr<EmbeddedVector> PTE5::emb_aa;
std::unique_ptr<EmbeddedVector> PTE5::emb_pj;
std::unique_ptr<EmbeddedVector> PTE5::emb_all;

std::unique_ptr<Sigmoid> PTE5::sigmoid;

int64_t PTE5::total_samples;

int64_t PTE5::current_sample_count;

float PTE5::cur_rho;

PTE5::PTE5(
        std::string pp_net_a,
        std::string pw_net_a,
        std::string pa_net_a,
        std::string aa_net_a,
        std::string pj_net_a,
        std::string pretrain_wvec_a,
        std::string outputfile_a,
        int binary_a,
        int size_a,
        int order_a,
        int negative_a,
        int64_t samples_a,
        int threads_a,
        float rho_a
)
{

    // 定数群
    constant_settings = std::unique_ptr<ConstantSettings>( new ConstantSettings(pp_net_a,pw_net_a,pa_net_a,aa_net_a,pj_net_a,pretrain_wvec_a,outputfile_a,binary_a,size_a,order_a,negative_a,samples_a,threads_a,rho_a) );

    current_sample_count = 0;
    cur_rho = constant_settings->rho;

}

PTE5::~PTE5() {
}

void PTE5::run() {

    total_samples = this->constant_settings->samples * 1000000;

    if (constant_settings->order != 1 && constant_settings->order != 2) {
        printf("Error: order should be either 1 or 2!\n");
        exit(1);
    }
    printf("--------------------------------\n");
    printf("Order: %d\n", constant_settings->order);
    printf("Samples: %lldM\n", total_samples / 1000000);
    printf("Negative: %d\n", constant_settings->negative);
    printf("Dimension: %d\n", constant_settings->dim);
    printf("Initial rho: %lf\n", constant_settings->rho);
    printf("--------------------------------\n");

    // networkの作成
    std::vector<std::string> networkfiles;
    networkfiles.push_back(this->constant_settings->pp_net);
    networkfiles.push_back(this->constant_settings->pw_net);
    networkfiles.push_back(this->constant_settings->pa_net);
    networkfiles.push_back(this->constant_settings->aa_net);
    networkfiles.push_back(this->constant_settings->pj_net);

    network_pp = std::unique_ptr<Network>(new Network(networkfiles[0], "pp"));
    network_pw = std::unique_ptr<Network>(new Network(networkfiles[1], "pw"));
    network_pa = std::unique_ptr<Network>(new Network(networkfiles[2], "pa"));
    network_aa = std::unique_ptr<Network>(new Network(networkfiles[3], "aa"));
    network_pj = std::unique_ptr<Network>(new Network(networkfiles[4], "pj"));
    network_all = std::unique_ptr<Network>(new NetworkAll(networkfiles));

    // samplerの作成
    sampler_pp = std::unique_ptr<Samplers>(new Samplers(network_pp.get()));
    sampler_pw = std::unique_ptr<Samplers>(new Samplers(network_pw.get()));
    sampler_pa = std::unique_ptr<Samplers>(new Samplers(network_pa.get()));
    sampler_aa = std::unique_ptr<Samplers>(new Samplers(network_aa.get()));
    sampler_pj = std::unique_ptr<Samplers>(new Samplers(network_pj.get()));

    // embeddingの作成
    emb_pp = std::unique_ptr<EmbeddedVector>(new EmbeddedVector(network_pp->num_vertices, this->constant_settings->dim));
    emb_pw = std::unique_ptr<EmbeddedVector>(new EmbeddedVector(network_pw->num_vertices, this->constant_settings->dim));
    emb_pa = std::unique_ptr<EmbeddedVector>(new EmbeddedVector(network_pa->num_vertices, this->constant_settings->dim));
    emb_aa = std::unique_ptr<EmbeddedVector>(new EmbeddedVector(network_aa->num_vertices, this->constant_settings->dim));
    emb_pj = std::unique_ptr<EmbeddedVector>(new EmbeddedVector(network_pj->num_vertices, this->constant_settings->dim));
    emb_all = std::unique_ptr<EmbeddedVector>(new EmbeddedVector(network_all->num_vertices, this->constant_settings->dim));

    // pretrained word vecファイルの読み込み
    std::cout << "pretrain_wvec: " << this->constant_settings->pretrain_wvec << std::endl;
    if(this->constant_settings->pretrain_wvec != "NULL") {
        pretrained_word_vectors = std::unique_ptr<PretrainedVectors>(
                new PretrainedVectors(this->constant_settings->pretrain_wvec, this->constant_settings->dim, "wordvec"));
        // pretrained vector によって上書き
//        std::cout << "network_pw->vertices[0]->name: " << network_pw->vertices[0]->name << std::endl;
        for(auto itr = pretrained_word_vectors->wvecdic.begin(); itr != pretrained_word_vectors->wvecdic.end(); itr++ ) {
            std::string word = itr->first;
            std::vector<float> vec = itr->second;
//            std::cout << "now word: " << word << std::endl;
            int idx = network_pw->searchHashTable(word);
//            std::cout << "idx: " << idx << std::endl;
            if(idx != -1) {
//                std::cout << "HIT! "    << word << std::endl;
                for(int i =0 ; i < emb_pw->dim; i++) {
                    emb_pw->vertex[idx][i] = vec[i];
                }
            }
//            std::cout << "pv word val[0] val[299]: "    << itr->first << " " << itr->second[0] << " " << itr->second[299] << std::endl;
        }
    }

    sigmoid = std::unique_ptr<Sigmoid>(new Sigmoid());

    std::cout << "total_samples:" << total_samples << std::endl;

    clock_t start;
    start = clock();
    printf("--------------------------------\n");

    // マルチプロセス
    std::thread* th = new std::thread[constant_settings->threads];
    for (int i=0; i< constant_settings->threads; i++) {
        th[i] = std::thread(&PTE5::trainPTEThread, i);
    }

    for (int i=0; i < constant_settings->threads; i++) {
        th[i].join();
    }
    std::cout << "complete" << std::endl;

    std::cout << std::endl;
    clock_t finish = clock();

    std::cout << "Total time: " << (double)(finish - start) / CLOCKS_PER_SEC << std::endl;

    this->output();
}

/* Update embeddings */
void PTE5::update(std::vector<float>& vec_u, std::vector<float>& vec_v, std::vector<float>& vec_error, int label) {
    // update の式にw_ijのエッジの重みがついてないのは,サンプルを繰り返すうちに重みの分の比で更新されていくという発想だと思われる.
    float x = 0;
    float g;


    //内積の計算
    for (int c = 0; c != constant_settings->dim; c++) {
        x += vec_u[c] * vec_v[c];
    }

//    if(std::isnan(x)) {
//        for (int c = 0; c != constant_settings->dim; c++) {
//            std::cout << "c:" << c << ", vec_u[c]:" << vec_u[c] << ", vec_v[c]:"<< vec_v[c] <<std::endl;
//        }
//    }

    g = - (label - sigmoid->fastFunc(x)) * cur_rho;
    for (int c = 0; c != constant_settings->dim; c++) {
        vec_error[c] += g * vec_v[c];
    }

    for (int c = 0; c != constant_settings->dim; c++) {
        vec_v[c] -= g * vec_u[c];
    }
}

void PTE5::trainPTEThread(int id) {
    int64_t count = 0;
    int64_t last_count = 0;
    int64_t last_count1 = 0;
    uint64_t seed = (uint64_t) (int64_t)id;
    int n_digit = 0;

    while (1) {
        // judge for exit
        // total_samples=1Mがベース
        if (count > total_samples / constant_settings->threads + 2) {
            break;
        }

        // 途中経過の出力
////        int now_digit = (int)std::log10((float)count);
//        if (count == 0) {
//            std::cout << "output_tmp: " << count << std::endl;
//            PTE5::output_tmp(count);
//
////        } else if (n_digit+1==now_digit) {
//        } else if (count - last_count1 > 99999 ) {
//            std::cout << "output_tmp: " << count << std::endl;
//            last_count1 = count;
////            n_digit = now_digit;
//            PTE5::output_tmp(count);
//        }

        // 表示の調整
        if (count - last_count>9999) {
//            std::unique_lock<std::mutex> uniq_lk(mtx);
            current_sample_count += count - last_count;
            last_count = count;
            printf("%cRho: %f  Progress: %.3lf%%", 13, cur_rho, (float)current_sample_count / (float)(total_samples + 1) * 100);
            fflush(stdout);
            // rho:学習率,init_rho=0.025サンプル数に応じて下げている
            cur_rho = constant_settings->rho * (1 - current_sample_count / (float)(total_samples + 1));
            // rhoの下限設定
            if (cur_rho < constant_settings->rho * 0.0001) {
                cur_rho = constant_settings->rho * 0.0001;
            }
        }

        // joint training
        Network* n_pp = network_pp.get();
        Network* n_pw = network_pw.get();
        Network* n_pa = network_pa.get();
        Network* n_aa = network_aa.get();
        Network* n_pj = network_pj.get();
        Network* n_all = network_all.get();

        EmbeddedVector* e_pp = emb_pp.get();
        EmbeddedVector* e_pw = emb_pw.get();
        EmbeddedVector* e_pa = emb_pa.get();
        EmbeddedVector* e_aa = emb_aa.get();
        EmbeddedVector* e_pj = emb_pj.get();
        EmbeddedVector* e_all = emb_all.get();
        Samplers* s_pp = sampler_pp.get();
        Samplers* s_pw = sampler_pw.get();
        Samplers* s_pa = sampler_pa.get();
        Samplers* s_aa = sampler_aa.get();
        Samplers* s_pj = sampler_pj.get();

        learnVector(n_pp, e_pp, s_pp, n_all, e_all, seed);
        learnVector(n_pw, e_pw, s_pw, n_all, e_all, seed);
        learnVector(n_pa, e_pa, s_pa, n_all, e_all, seed);
        learnVector(n_aa, e_aa, s_aa, n_all, e_all, seed);
        learnVector(n_pj, e_pj, s_pj, n_all, e_all, seed);

        count++;

    }
}

void* PTE5::learnVector(
    Network*         network_each,
    EmbeddedVector*  emb_each,
    Samplers*        sampler_each,
    Network*         network_all_a,
    EmbeddedVector*  emb_all_a,
    uint64_t seed
) {

    int64_t curedge;
    int64_t s_id;
    int64_t t_id;
    std::string s_name;
    std::string t_name;

    int64_t s_all_id;
    int64_t t_all_id;
    std::string s_all_name;
    std::string t_all_name;

    std::vector<float> vec_error(constant_settings->dim);


    // edgeサンプリング（エッジのWeightに比例する形でAliasTableを利用してサンプリング）
    // for ww network
    curedge = sampler_each->edgeSampling();
    s_id    = network_each->edges[curedge]->source_id;
    t_id    = network_each->edges[curedge]->target_id;


    //s_idとt_idをランダムに入れ替える仕様に変更してみる
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> r_uni(0, 1);
    double rand = r_uni(mt);
    if(rand<0.5) {
        int64_t tmp_id = s_id;
        s_id = t_id;
        t_id = tmp_id;
    }

    s_name  = network_each->vertices[s_id]->name;
    t_name  = network_each->vertices[t_id]->name;

    s_all_id    = network_all_a->searchHashTable(s_name);
    t_all_id    = network_all_a->searchHashTable(t_name);
    s_all_name  = network_all_a->vertices[s_all_id]->name;
    t_all_name  = network_all_a->vertices[t_all_id]->name;

    // copy latest emb_all to emb_each
    for (int c = 0; c != constant_settings->dim; c++) {
        emb_each->vertex[s_id][c] = emb_all_a->vertex[s_all_id][c];
    }

    // vec_error, vec_error_allを０で初期化
    for (int c = 0; c != constant_settings->dim; c++) {
        vec_error[c] = 0;
    }

    int64_t target;
    int64_t label;
    // NEGATIVE SAMPLING
    for (int d = 0; d != constant_settings->negative + 1; d++) {
        if (d == 0) {
            target = t_id;
            label = 1;
        } else {
            target = sampler_each->negativeVertexSampling(seed);
            t_name = network_each->vertices[target]->name;
            t_all_id = network_all->searchHashTable(t_name);
            label = 0;

        }

        // copy latest emb_all to emb_each
        for (int c = 0; c != constant_settings->dim; c++) {
            emb_each->vertex[target][c]     = emb_all_a->vertex[t_all_id][c];
            emb_each->context[target][c]    = emb_all_a->context[t_all_id][c];
        }

        // 更新
        if (constant_settings->order == 1) {
            update(emb_each->vertex[s_id], emb_each->vertex[target], vec_error, label);
            for (int c = 0; c != constant_settings->dim; c++) {
                emb_all_a->vertex[t_all_id][c] = emb_each->vertex[target][c];
            }
        }


        if (constant_settings->order == 2) {
            update(emb_each->vertex[s_id], emb_each->context[target], vec_error, label);
            for (int c = 0; c != constant_settings->dim; c++) {
                emb_all_a->context[t_all_id][c] = emb_each->context[target][c];
            }
        }


    }

    for (int c = 0; c != constant_settings->dim; c++) {
        // original LINE code is mistaken perhaps, change + to -.
        emb_each->vertex[s_id][c] -= vec_error[c];
        emb_all_a->vertex[s_all_id][c] = emb_each->vertex[s_id][c];
    }
}

void PTE5::output() {
    FILE* fo = fopen(this->constant_settings->outputfile.c_str(), "wb");
    fprintf(fo, "%d %d\n", network_all->num_vertices, this->constant_settings->dim);
    for (int i = 0; i < network_all->num_vertices; i++) {
        std::string name = this->network_all->vertices[i]->name;
        fprintf(fo, "%s ", name.c_str());
        if (this->constant_settings->binary) {
            for (int j = 0; j < this->constant_settings->dim; j++) {
                fwrite(&emb_all->vertex[i][j], sizeof(float), 1, fo);
            }

        } else {
            for (int j = 0; j < this->constant_settings->dim; j++) {
                fprintf(fo, "%lf ", emb_all->vertex[i][j]);
            }
        }

        fprintf(fo, "\n");
    }
    fclose(fo);
}

void PTE5::output_tmp(int loop_c) {
//    std::unique_lock<std::mutex> uniq_lk(mtx);
    std::string outputfile_n = constant_settings->outputfile + std::to_string(loop_c);
    FILE* fo = fopen(outputfile_n.c_str(), "wb");
    fprintf(fo, "%d %d\n", network_all->num_vertices, constant_settings->dim);
    for (int i = 0; i < network_all->num_vertices; i++) {
        std::string name = network_all->vertices[i]->name;
        fprintf(fo, "%s ", name.c_str());
        if (constant_settings->binary) {
            for (int j = 0; j < constant_settings->dim; j++) {
                fwrite(&emb_all->vertex[i][j], sizeof(float), 1, fo);
            }

        } else {
            for (int j = 0; j < constant_settings->dim; j++) {
                fprintf(fo, "%lf ", emb_all->vertex[i][j]);
            }
        }

        fprintf(fo, "\n");
    }
    fclose(fo);
}

void PTE5::disp_vec(std::vector<float> vec) {

    int vec_size = vec.size();
    std::cout << "vec::";
    for(int i=0; i<vec_size; i++) {
        if(i>0) {
            std::cout << ", ";
        }
        std::cout << vec[i];

    }
    std::cout << std::endl;

}
