//
// Created by m-ochi on 16/03/15.
//

#ifndef PTESELF_EMBEDDEDVECTOR_H
#define PTESELF_EMBEDDEDVECTOR_H

#include <iostream>
#include <vector>


class EmbeddedVector {
public:
    EmbeddedVector(int num_vertices_a, int dim_a);
    ~EmbeddedVector();
    std::vector<std::vector<float>>  vertex;
    std::vector<std::vector<float>>  context;
    int num_vertices;
    int dim;

private:
    std::vector<std::vector<float>> InitEmb2DArray(int num_vertices, int dim);

};


#endif //PTESELF_EMBEDDEDVECTOR_H
