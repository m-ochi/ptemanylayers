//
// Created by m-ochi on 16/03/16.
//

#include "sigmoid.h"
#include "math.h"
#include <memory>
#include <iostream>


/* Fastly compute sigmoid function */
Sigmoid::Sigmoid(int sigmoid_table_size_a,int sigmoidbound_a)
:sigmoid_table_size(sigmoid_table_size_a), sigmoidbound((float)sigmoidbound_a)
{
    float x;
    sigmoid_table = std::unique_ptr<float[]>( new float[sigmoid_table_size+1] );
    for (int k = 0; k < sigmoid_table_size+1; k++) {
        // xは-6~+6でで定義されていて，予めシグモイド関数の値を計算しておく
        x = 2 * sigmoidbound * k / sigmoid_table_size - sigmoidbound;
        sigmoid_table[k] = 1 / (1 + exp(-x));
    }
}

Sigmoid::~Sigmoid(){}

float Sigmoid::fastFunc(float x) {
    if (x > sigmoidbound) return 1;
    else if (x < -sigmoidbound) return 0;
    int k = (x + sigmoidbound) * sigmoid_table_size / sigmoidbound / 2;
//    if (k < 0) {
//        k = 0;
//        std::cout << "x:" << x << ", k:" << k <<std::endl;
//    } else if (k > sigmoid_table_size) {
//        k = sigmoid_table_size;
//        std::cout << "x:" << x << ", k:" << k <<std::endl;
//    }
    return sigmoid_table[k];
}


/*
int main() {
    std::unique_ptr<Sigmoid> sig( new Sigmoid(10000) );
    for(int i=-50; i<50; i++) {
        float y = sig->fastFunc((float)i);
        std::cout << "x:" << i << ", y:" << y << std::endl;
    }


    return 0;
}
*/


