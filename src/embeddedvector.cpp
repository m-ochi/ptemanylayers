//
// Created by m-ochi on 16/03/15.
//

#include "embeddedvector.h"
#include <random>
#include <memory>

EmbeddedVector::EmbeddedVector(int num_vertices_a, int dim_a)
: num_vertices(num_vertices_a), dim(dim_a)
{
    this->vertex  = this->InitEmb2DArray(num_vertices, dim);
    this->context = this->InitEmb2DArray(num_vertices, dim);
}

EmbeddedVector::~EmbeddedVector() {}

std::vector<std::vector<float>> EmbeddedVector::InitEmb2DArray(int num_vertices, int dim) {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<float> r_uni(-0.5, 0.5);

    std::vector<std::vector<float> > emb_vertex;

    emb_vertex.resize(num_vertices);

    for(int64_t i=0; i<num_vertices; i++) {
        emb_vertex[i].resize(dim);
        for(int j=0; j<dim; j++) {
            emb_vertex[i][j] = r_uni(mt);
        }
    }
    return emb_vertex;
}


/*
int main() {
    std::unique_ptr<EmbeddedVector> emb( new EmbeddedVector(5,3) );

    std::cout << "emb->vertex:" << std::endl;
    for(int i=0; i<emb->num_vertices; i++){
        for(int j=0; j<emb->dim; j++) {
            if(j>0) {
                std::cout << ", ";
            }
            std::cout << emb->vertex[i][j];
        }
        std::cout << "" << std::endl;
    }

    std::cout << "emb->context:" << std::endl;
    for(int i=0; i<emb->num_vertices; i++){
        for(int j=0; j<emb->dim; j++) {
            if(j>0) {
                std::cout << ", ";
            }
            std::cout << emb->context[i][j];
        }
        std::cout << "" << std::endl;
    }

    return 0;
}
*/

