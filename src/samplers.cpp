//
// Created by m-ochi on 16/03/15.
//

#include "samplers.h"
#include <iostream>

Samplers::Samplers(Network* network_a, double neg_power_a)
: network(network_a), neg_power(neg_power_a)
{
    int     num_vertices    = network->num_vertices;
    int64_t num_edges       = network->num_edges;

    edgeAliasTable          = this->makeEdgeAliasTable();
    neg_vertexAliasTable    = this->makeVertexAliasTable();

}

Samplers::~Samplers() {
//    std::cout << "DELETE A Sampler Class." << std::endl;
}

std::unique_ptr<AliasTable> Samplers::makeVertexAliasTable() {
    int64_t num_vertices       = this->network->num_vertices;
    std::unique_ptr<double[]> neg_vertexDegrees = this->getVertexDegrees(neg_power);
    std::unique_ptr<AliasTable> vertexAliasTable(new AliasTable(num_vertices, neg_vertexDegrees.get()));
    return std::move(vertexAliasTable);
}

std::unique_ptr<double[]> Samplers::getVertexDegrees(double neg_power) {
    int64_t num_vertices    = this->network->num_vertices;
    std::unique_ptr<double[]> vertexDegrees( new double[num_vertices] );
    for(int i=0; i<num_vertices; i++){
        Vertex* anVertex = this->network->vertices[i].get();
        vertexDegrees[i] = pow(anVertex->degree, neg_power);
    }
    return std::move(vertexDegrees);
}


std::unique_ptr<AliasTable> Samplers::makeEdgeAliasTable() {
    int64_t num_edges       = this->network->num_edges;
    std::unique_ptr<double[]> edgeWeights = this->getEdgeWeights();
    std::unique_ptr<AliasTable> edgeAliasTable(new AliasTable(num_edges, edgeWeights.get()));
    return std::move(edgeAliasTable);
}

std::unique_ptr<double[]> Samplers::getEdgeWeights() {
    int64_t num_edges    = this->network->num_edges;
    std::unique_ptr<double[]> edgeWeights( new double[num_edges] );
    for(int i=0; i<num_edges; i++){
        Edge* anEdge = this->network->edges[i].get();
        edgeWeights[i] = anEdge->weight;
    }

    return std::move(edgeWeights);
}

int64_t Samplers::edgeSampling(uint64_t seed) {
    int64_t edge_id = this->edgeAliasTable->sampling(0,0,seed);
    return edge_id;
}

int Samplers::negativeVertexSampling(uint64_t seed) {
    int vertex_id = this->neg_vertexAliasTable->sampling(0,0,seed);
    return vertex_id;
}


/*
int main () {
    std::unique_ptr<Network> network( new Network("dataminimal/st-st.csv", "ww") );
    std::unique_ptr<Samplers> smp(new Samplers(network.get(),0.5));

    for(int i=0; i<1000000; i++) {
        int64_t e_id = smp->edgeSampling();
        int s_id = network->edges[e_id]->source_id;
        int t_id = network->edges[e_id]->target_id;
        double w = network->edges[e_id]->weight;
        std::cout << "\"" << w << "\"" << std::endl;
    }

//    for(int i=0; i<1000000; i++) {
//        int64_t v_id = smp->negativeVertexSampling();
//        double d = network->vertices[v_id]->degree;
//        std::cout << "\"" << d << "\"" << std::endl;
//    }


    return 0;
}
*/

