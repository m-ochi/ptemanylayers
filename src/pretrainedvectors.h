//
// Created by m-ochi on 18/07/09.
// Pretrained Word Vectorの特徴量の格納(学習するベクトルデータは別)
//

#ifndef PTESELF_PRETRAINEDVECTORS_H
#define PTESELF_PRETRAINEDVECTORS_H

#include <iostream>
#include <memory>
#include <vector>
#include <unordered_map>
#include <string.h>
#include <cstdint>

class PretrainedVectors {
public:
    //
    PretrainedVectors(std::string wvec_file, int dim, std::string mode);
    PretrainedVectors();
    ~PretrainedVectors();
    // property

    int     num_words;
    int     word_dim;
    std::unordered_map<std::string, std::vector<float>> wvecdic;

    // method


protected:
    // property

    // method
    void readFromFile(std::string wvec_file, int word_dim, std::string mode);
};

#endif //PTESELF_PRETRAINEDVECTORS_H
