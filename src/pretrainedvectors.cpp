//
// Created by m-ochi on 18/07/09.
//

#include "pretrainedvectors.h"

#define MAX_STRING 1000000

PretrainedVectors::PretrainedVectors(std::string wvec_file, int dim, std::string mode) {

    // property
    word_dim = dim;

    // init
    readFromFile(wvec_file, word_dim, mode);

}

PretrainedVectors::PretrainedVectors() { }

PretrainedVectors::~PretrainedVectors() { }

void PretrainedVectors::readFromFile(std::string wvec_file, int word_dim, std::string mode) {
    FILE *fin;
    char raw_string[MAX_STRING];
    char str[2 * MAX_STRING + 10000];

    fin = fopen(wvec_file.c_str(), "rb");
    if (fin == NULL) {
        printf("ERROR: wordvec file is not found!\n");
        exit(1);
    }

    this->num_words = 0;

    //fgets:ファイルポインタから１行読み込む
    while (fgets(str, sizeof(str), fin)) {
        (this->num_words)++;
    }

    fclose(fin);
//    std::cout << "Number of words " << mode << ": " << this->num_words << std::endl;

    fin = fopen(wvec_file.c_str(), "rb");
    for (int k = 0; k < this->num_words; k++) {
        fgets(raw_string, sizeof(raw_string),fin);
//        std::cout << k << std::endl;

        // read space delimitered file
        std::vector<std::string> elems;
        std::string item;
        for (char ch: raw_string) {
            if (ch == ' ') {
                if (!item.empty()) {
                    elems.push_back(item);
                }
                item.clear();

            } else {
                item += ch;
            }
        }
        if (!item.empty()) {
            elems.push_back(item);
        }

        char name_word_char[MAX_STRING];
        std::string name_word;
        std::char_traits<char>::copy(name_word_char, elems[0].c_str(), elems[0].size()+1);
        name_word = std::string(name_word_char);
//        std::cout << "name_word: " << name_word << std::endl;
        std::vector<float> wvec(word_dim);
        for(int j=0; j<word_dim; j++) {
            wvec[j] = std::stof(elems[j+1]);
        }
        this->wvecdic[name_word] = wvec;
//        std::cout << "this->wvecdic[" << name_word << "][0]: " <<  this->wvecdic[name_word][0] << std::endl;
//        std::cout << "this->wvecdic[" << name_word << "][299]: " <<  this->wvecdic[name_word][299] << std::endl;

        if (k % 10000 == 0) {
            printf("Reading words %s: %.3lf%%%c", mode.c_str(), k / (double)(this->num_words + 1) * 100, 13);
            fflush(stdout);
        }

    }
    fclose(fin);

    printf("Number of words for pretrained %s: %d          \n", mode.c_str(), this->num_words);

}

/*
int main() {

    std::unique_ptr<PretrainedVectors> pv( new PretrainedVectors("../dataminimal/sample.vec", 300, "wvec") );

    std::cout << "pretrainedvectors->num_words:"    << pv->num_words     << std::endl;

    for(auto itr = pv->wvecdic.begin(); itr != pv->wvecdic.end(); itr++ ) {
        std::cout << "pv word val[0] val[299]: "    << itr->first << " " << itr->second[0] << " " << itr->second[299] << std::endl;
    }

    std::string q = "death";
    if(pv->wvecdic.find(q) != pv->wvecdic.end() ) {
        std::cout << "query: "    << q << " found." << std::endl;
    } else {
        std::cout << "query: "    << q << " did not find." << std::endl;
    }

    q = "roager";
    if(pv->wvecdic.find(q) != pv->wvecdic.end() ) {
        std::cout << "query: "    << q << " found." << std::endl;
    } else {
        std::cout << "query: "    << q << " did not find." << std::endl;
    }

    return 0;
}
 */
