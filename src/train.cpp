#include "cmdline/cmdline.h"
#include "pte5.h"
#include <iostream>
#include <string>

int main(int argc, char **argv) {
    cmdline::parser option;
    option.add<std::string>("train_pp",0,"set paper--paper network csv file (citation network)",true);
    option.add<std::string>("train_pw",0,"set paper--word network csv file",true);
    option.add<std::string>("train_pa",0,"set paper--author network csv file",true);
    option.add<std::string>("train_aa",0,"set author--affiliation network csv file",true);
    option.add<std::string>("train_pj",0,"set paper--journal network csv file",true);
    option.add<std::string>("pretrain_wvec",0,"set pre-trained word vector file",false, "NULL");
    option.add<std::string>("output",  0,"set output file",false,"out_embeddings.txt");
    option.add<int>("binary",  0,"save the learnt embeddings in binary mode; default is 0(off)",false,0);
    option.add<int>("size",    0,"Set dimension of vertex embeddings; default is ",false,300);
    option.add<int>("order",   0,"The type of the model; 1 for first order, 2 for second order; default is ",false,2);
    option.add<int>("negative",0,"Number of negative examples; default is ",false,5);
    option.add<int>("samples", 0,"Set the number of training samples as <int>Million; default is ",false,1);
    option.add<int>("threads", 0,"Use <int> threads (default is )",false,1);
    option.add<float>("rho", 0,"Set the starting learning rate; default is ",false,0.025);

    option.parse_check(argc,argv);

    std::string train_pp = option.get<std::string>("train_pp");
    std::string train_pw = option.get<std::string>("train_pw");
    std::string train_pa = option.get<std::string>("train_pa");
    std::string train_aa = option.get<std::string>("train_aa");
    std::string train_pj = option.get<std::string>("train_pj");
    std::string pretrain_wvec = option.get<std::string>("pretrain_wvec");
    std::string output   = option.get<std::string>("output");
    int binary  = option.get<int>("binary");
    int size    = option.get<int>("size");
    int order   = option.get<int>("order");
    int negative= option.get<int>("negative");
    int samples = option.get<int>("samples");
    int threads = option.get<int>("threads");
    float rho   = option.get<float>("rho");

//    std::cout << option.get<std::string>("train_pp") << std::endl;
//    std::cout << option.get<std::string>("train_pw") << std::endl;
//    std::cout << option.get<std::string>("train_pa") << std::endl;
//    std::cout << option.get<std::string>("train_aa") << std::endl;
//    std::cout << option.get<std::string>("pretrain_wvec") << std::endl;
//    std::cout << option.get<std::string>("output") << std::endl;
//    std::cout << option.get<int>("binary") << std::endl;
//    std::cout << option.get<int>("size") << std::endl;
//    std::cout << option.get<int>("order") << std::endl;
//    std::cout << option.get<int>("negative") << std::endl;
//    std::cout << option.get<int>("samples") << std::endl;
//    std::cout << option.get<int>("threads") << std::endl;
//    std::cout << option.get<float>("rho") << std::endl;

    std::unique_ptr<PTE5> pte(new PTE5(train_pp,train_pw,train_pa,train_aa,train_pj,pretrain_wvec,output,binary,size,order,negative,samples,threads,rho));
    pte->run();
}

