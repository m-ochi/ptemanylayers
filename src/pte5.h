#ifndef INCLUDED_pte_h_
#define INCLUDED_pte_h_

#include <iostream>
#include <string>
#include <cstdint>
#include <thread>
#include <memory>
#include "aliasmethod/aliastable.h"
#include "network.h"
#include "networkall.h"
#include "pretrainedvectors.h"
#include "samplers.h"
#include "embeddedvector.h"
#include "constantsettings.h"
#include "sigmoid.h"
#include <random>
#include <limits>


class PTE5 {
protected:
    // 定数群
    static std::unique_ptr<ConstantSettings> constant_settings;

    static std::unique_ptr<Network> network_pp;
    static std::unique_ptr<Network> network_pw;
    static std::unique_ptr<Network> network_pa;
    static std::unique_ptr<Network> network_aa;
    static std::unique_ptr<Network> network_pj;
    static std::unique_ptr<Network> network_all;

    static std::unique_ptr<PretrainedVectors> pretrained_word_vectors;

    static std::unique_ptr<Samplers> sampler_pp;
    static std::unique_ptr<Samplers> sampler_pw;
    static std::unique_ptr<Samplers> sampler_pa;
    static std::unique_ptr<Samplers> sampler_aa;
    static std::unique_ptr<Samplers> sampler_pj;

    static std::unique_ptr<EmbeddedVector> emb_pp;
    static std::unique_ptr<EmbeddedVector> emb_pw;
    static std::unique_ptr<EmbeddedVector> emb_pa;
    static std::unique_ptr<EmbeddedVector> emb_aa;
    static std::unique_ptr<EmbeddedVector> emb_pj;
    static std::unique_ptr<EmbeddedVector> emb_all;

    static std::unique_ptr<Sigmoid> sigmoid;

    //method
    static void* learnVector(Network* network_each, EmbeddedVector* emb_each, Samplers* sampler_each, Network* network_all_a, EmbeddedVector* emb_all_a, uint64_t seed);

    static int64_t total_samples;
    static int64_t current_sample_count;
    static float cur_rho;

    static void update(std::vector<float>& vec_u, std::vector<float>& vec_v, std::vector<float>& vec_error, int label);
    static void trainPTEThread(int id);

    virtual void output();
    static void output_tmp(int loop_c);
    static void disp_vec(std::vector<float>);

public:
    PTE5(
        std::string pp_net_a,
        std::string pw_net_a,
        std::string pa_net_a,
        std::string aa_net_a,
        std::string pj_net_a,
        std::string pretrain_wvec_a,
        std::string outputfile_a,
        int binary_a=0,
        int size_a=100,
        int order_a=2,
        int negative_a=5,
        int64_t samples_a=1,
        int threads_a=1,
        float rho_a=0.025
    );
    ~PTE5();

    virtual void run();

};

#endif
