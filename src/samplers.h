//
// Created by m-ochi on 16/03/15.
//

#ifndef PTESELF_SAMPLERS_H
#define PTESELF_SAMPLERS_H

#include <iostream>
#include "aliasmethod/aliastable.h"
#include "network.h"
#include <string>

class Samplers {
public:
    Samplers(Network* network_a, double neg_power_a=0.75);
    ~Samplers();
    int64_t edgeSampling(uint64_t seed=131);
    int negativeVertexSampling(uint64_t seed=131);
    std::string line;


private:
    Network* network;
    double neg_power;
    std::unique_ptr<AliasTable> edgeAliasTable;
    std::unique_ptr<AliasTable> neg_vertexAliasTable;

    std::unique_ptr<double[]> getEdgeWeights();
    std::unique_ptr<AliasTable> makeEdgeAliasTable();

    std::unique_ptr<double[]> getVertexDegrees(double neg_power);
    std::unique_ptr<AliasTable> makeVertexAliasTable();

};


#endif //PTESELF_SAMPLERS_H
