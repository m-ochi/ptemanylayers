//
// Created by m-ochi on 16/03/16.
//

#ifndef PTESELF_SIGMOID_H
#define PTESELF_SIGMOID_H

#include <memory>

class Sigmoid {

public:
    Sigmoid(int sigmoid_table_size_a=1000, int sigmoidbound_a=6);
    ~Sigmoid();
    float fastFunc(float x);

private:
    int sigmoid_table_size;
    float sigmoidbound;
    std::unique_ptr<float[]> sigmoid_table;
};


#endif //PTESELF_SIGMOID_H
