#!/bin/sh

#cmake .
#make clean
#make

./train_ptemanylayers \
  --train_pp=data/p-p.csv \
  --train_pw=data/p-w.csv \
  --train_pa=data/p-a.csv \
  --train_aa=data/au-aff.csv \
  --train_pj=data/p-j.csv \
  --pretrain_wvec=dataminimal/sample.vec \
  --output=vec_2nd_wo_norm.txt \
  --binary=0 \
  --size=300 \
  --order=1 \
  --negative=5 \
  --samples=1 \
  --threads=1 \
  --rho=0.05

